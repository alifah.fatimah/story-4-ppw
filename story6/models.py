from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=100)
    deskripsi_kegiatan = models.CharField(max_length=1000)
    def __str__(self):
        return self.nama_kegiatan
        
class Orang(models.Model):
    nama_orang = models.CharField(max_length=64)
    kegiatan = models.ForeignKey(Kegiatan,
                                 on_delete=models.CASCADE, null=True, blank=True)
    def __str__(self):
        return self.nama_orang
