from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import FormKegiatan, FormOrang
from .models import Kegiatan, Orang 
from story6 import forms

# Create your views here.
# def index(request):
#     return render(request, 'index.html')

def activities(request):
    return render(request, 'activities.html')


def addnew(request):
    form_kegiatan = forms.FormKegiatan(request.POST or None)
    if request.method == 'POST':
        if form_kegiatan.is_valid():
            data = form_kegiatan.cleaned_data
            kegiatan_input = Kegiatan()
            kegiatan_input.nama_kegiatan = data['nama_kegiatan']
            kegiatan_input.save()
            return redirect('story6:listActivity')
        else:
            current_data = Kegiatan.objects.all().values()
            return render(request, 'addnew.html',{'form':form_kegiatan, 'status':'failed','data':current_data})
    else:
        current_data = Kegiatan.objects.all()
        return render(request, 'addnew.html',{'form':form_kegiatan,'data':current_data})

def register(request, keg_id):
    form_orang = forms.FormOrang(request.POST or None)
    if request.method == 'POST':
        if form_orang.is_valid():
            data = form_orang.cleaned_data
            orangBaru = Orang()
            orangBaru.nama_orang = data['nama_orang']
            orangBaru.kegiatan = Kegiatan.objects.get(id=keg_id)
            orangBaru.save()
            print(data)
            return redirect('story6:listActivity')
        else:
            return render(request, 'register.html', {'form': form_orang, 'status': 'failed'})
    else:
        return render(request, 'register.html', {'form': form_orang})


def listActivity(request):
    kegiatan = Kegiatan.objects.all().values()
    orang = Orang.objects.all().values()
    print(orang)
    return render(request, 'list.html', {'kegiatan': kegiatan, 'orang': orang})


def deleteUser(request, delete_id):
    # if request.method == "POST"
    orang_to_delete = Orang.objects.get(id=delete_id)
    orang_to_delete.delete()
    # return render(request, 'story6/listActivity.html', {'kegiatan': kegiatan, 'orang': orang})
    return redirect('story6:listActivity')

