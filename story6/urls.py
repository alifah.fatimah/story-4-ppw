from django.urls import path, include

from story6 import views

app_name = 'story6'
urlpatterns = [
    path('activities', views.activities, name='activities'),
    path('addnew', views.addnew, name='addnew'),
    path('listActivity', views.listActivity, name='listActivity'),
    path('register/<int:keg_id>/', views.register, name='register'),
    path('delete/<int:delete_id>/', views.deleteUser, name='delete'),
]