from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .models import Kegiatan, Orang
from .forms import FormKegiatan, FormOrang
from .views import addnew, listActivity, deleteUser, register
from .apps import Story6Config


class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar")
        self.orang = Orang.objects.create(nama_orang="alipeh")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Orang.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "belajar")
        self.assertEqual(str(self.orang), "alipeh")

# Create your tests here.
class FormTest(TestCase):

    def test_form_is_valid(self):
        form_kegiatan = FormKegiatan(data={
            "nama_kegiatan": "belajar",
        })
        self.assertTrue(form_kegiatan.is_valid())
        form_orang = FormOrang(data={
            'nama_orang': "alipeh"
        })
        self.assertTrue(form_orang.is_valid())

    def test_form_invalid(self):
        form_kegiatan = FormKegiatan(data={})
        self.assertFalse(form_kegiatan.is_valid())
        form_orang = FormOrang(data={})
        self.assertFalse(form_orang.is_valid())

class UrlsTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar")
        self.orang = Orang.objects.create(
            nama_orang="Joni", kegiatan=Kegiatan.objects.get(nama_kegiatan="belajar"))
        # self.index = reverse("index")
        self.listActivity = reverse("story6:listActivity")
        self.addnew = reverse("story6:addnew")
        self.register = reverse("story6:register", args=[self.kegiatan.pk])
        self.delete = reverse("story6:delete", args=[self.orang.pk])

    # def test_index_use_right_function(self):
    #     found = resolve(self.index)
    #     self.assertEqual(found.func, index)

    def test_listActivity_use_right_function(self):
        found = resolve(self.listActivity)
        self.assertEqual(found.func, listActivity)

    def test_addnew_use_right_function(self):
        found = resolve(self.addnew)
        self.assertEqual(found.func, addnew)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, register)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, deleteUser)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        # self.index = reverse("index")
        self.listActivity = reverse("story6:listActivity")
        self.addnew = reverse("story6:addnew")

    # def test_GET_index(self):
    #     response = self.client.get(self.index)
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'index.html'

    def test_GET_listActivity(self):
        response = self.client.get(self.listActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'list.html')

    def test_GET_addnew(self):
        response = self.client.get(self.addnew)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addnew.html')

    def test_POST_addnew(self):
        response = self.client.post(self.addnew,
                                    {
                                        'nama_kegiatan': 'belajar',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addnew_invalid(self):
        response = self.client.post(self.addnew,
                                    {
                                        'nama_kegiatan': '',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'addnew.html')

    def test_GET_delete(self):
        kegiatan = Kegiatan(nama_kegiatan="abc")
        kegiatan.save()
        orang = Orang(nama_orang="mangoleh",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('story6:delete', args=[orang.pk]))
        self.assertEqual(Orang.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="abc")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/story6/register/1/',
                                 data={'nama_orang': 'bangjago'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/story6/register/1/')
        self.assertTemplateUsed(response, 'register.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/story6/register/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'register.html')
        # self.assertEqual(response.status_code, 302)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')
