from django import forms


class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(
        label="Nama kegiatan",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

class FormOrang(forms.Form):
    nama_orang = forms.CharField(
        label="Name",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

