from django import forms
YEAR = [ ('Gasal 2019/2020', 'Gasal 2019/2020'),
         ('Genap 2019/2020', 'Genap 2019/2020'),
         ('Gasal 2020/2021', 'Gasal 2020/2021'),
         ('Genap 2020/2021', 'Genap 2020/2021'),
         ('Gasal 2021/2022', 'Gasal 2021/2022'),
         ('Genap 2021/2022', 'Genap 2021/2022'),
         ]
class FormMatkul(forms.Form):
    course = forms.CharField(
        label = "Course:", 
        max_length = 50,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'SDA, PPW, etc.',
                'required':True
            }
        )
    )

    sks = forms.IntegerField(
        label = "Credits",
        widget = forms.NumberInput(
            attrs={
                'placeholder':'Credits',
                'class':'form-control',
                'min':'1',
                'max':'6',
                'required': True
            }
        )
    )

    room = forms.CharField(
        label = "Room", 
        max_length = 50,
        widget = forms.TextInput(
            attrs={
                'placeholder':'Ex: 3.3114',
                'class':'form-control',
                'required':True
            }
        )
    )

    dosen = forms.CharField(
        label = "Lecturer", 
        max_length = 50,
        widget = forms.TextInput(
            attrs={
                'placeholder':'Lecturer',
                'class':'form-control',
                'required':True
            }
        )
    )
    semester = forms.CharField(
        label = "Semester",
        widget = forms.Select(choices=YEAR)
    )

    description = forms.CharField(
        label = "Course Description", 
        max_length = 75,
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
                'placeholder':'Describe your course here...',
                'required':True
            }
        )
    )
