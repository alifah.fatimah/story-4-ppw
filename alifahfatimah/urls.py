from django.urls import path, include
from .views import index
from alifahfatimah import views

urlpatterns = [
    path('', index, name='index'),
    path('interests/', views.interests, name='interests'),
    path('gallery/', views.gallery, name='gallery'),
    path('schedule/', views.schedule, name='schedule'),
    path('daftar/', views.daftar, name='daftar'),
    path('add/', views.add, name='add'),
    path('result/<int:id>',views.remove, name = 'remove'),
]