from django.shortcuts import render, redirect
from django.http import HttpResponse 
from alifahfatimah import forms
from .models import MataKuliah
# Create your views here.

def index(request):
    return render(request, 'index.html')

def interests(request):
    return render(request, 'single.html')

def gallery(request):
    return render(request, 'gallery.html')

def schedule(request):
    return render(request, 'schedule.html')

def add(request):
    form_course = forms.FormMatkul(request.POST or None)
    if request.method == 'POST':
        if form_course.is_valid():
            data = form_course.cleaned_data
            matkul_input = MataKuliah()
            matkul_input.course = data['course']
            matkul_input.sks = data['sks']
            matkul_input.room = data['room']
            matkul_input.dosen = data['dosen']
            matkul_input.semester = data['semester']
            matkul_input.description = data['description']
            matkul_input.save()
            return redirect("daftar")
        else:
            current_data = MataKuliah.objects.all().values()
            return render(request, 'add.html',{'form':form_course, 'status':'failed','data':current_data})
    else:
        current_data = MataKuliah.objects.all()
        return render(request, 'add.html',{'form':form_course,'data':current_data})

def daftar(request):
    data = MataKuliah.objects.all().values()
    return render(request,'daftar.html',{'listMatkul':data})

def remove(request, id):
    MataKuliah.objects.filter(id=id).delete()
    return redirect('daftar')