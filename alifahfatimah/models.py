from django.db import models


# Create your models here.
YEAR = [ ('Gasal 2019/2020', 'Gasal 2019/2020'),
         ('Genap 2019/2020', 'Genap 2019/2020'),
         ('Gasal 2020/2021', 'Gasal 2020/2021'),
         ('Genap 2020/2021', 'Genap 2020/2021'),
         ('Gasal 2021/2022', 'Gasal 2021/2022'),
         ('Genap 2021/2022', 'Genap 2021/2022'),
]

class MataKuliah(models.Model):
    course = models.CharField(max_length = 50)
    sks = models.IntegerField()
    room = models.CharField(max_length = 50)
    dosen = models.CharField(max_length = 50)
    semester = models.CharField(max_length = 50, choices=YEAR, default='0000000')
    description = models.CharField(max_length = 75, null = True)

    def __str__(self):
        return self.course



