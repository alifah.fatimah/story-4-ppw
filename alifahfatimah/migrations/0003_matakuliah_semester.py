# Generated by Django 3.1.2 on 2020-10-13 16:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alifahfatimah', '0002_auto_20201013_1617'),
    ]

    operations = [
        migrations.AddField(
            model_name='matakuliah',
            name='semester',
            field=models.CharField(choices=[('Gasal 2019/2020', 'Gasal 2019/2020'), ('Genap 2019/2020', 'Genap 2019/2020'), ('Gasal 2020/2021', 'Gasal 2020/2021'), ('Genap 2020/2021', 'Genap 2020/2021'), ('Gasal 2021/2022', 'Gasal 2021/2022'), ('Genap 2021/2022', 'Genap 2021/2022')], default='0000000', max_length=50),
        ),
    ]
