# Generated by Django 3.1.2 on 2020-10-13 10:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MataKuliah',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('course', models.CharField(max_length=50)),
                ('sks', models.IntegerField()),
                ('room', models.CharField(max_length=50)),
                ('dosen', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=75, null=True)),
            ],
        ),
    ]
