from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .views import listAccordian


class UrlsTest(TestCase):
    def test_listAccordian_use_right_function(self):
        self.listAccordian = reverse("story7:listAccordian")
        found = resolve(self.listAccordian)
        self.assertEqual(found.func, listAccordian)

class ViewsTest(TestCase):
    def test_GET_listAccordian(self):
        self.listAccordian = reverse("story7:listAccordian")
        response = self.client.get(self.listAccordian)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listAccordian.html')