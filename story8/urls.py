from django.urls import path
from story8 import views
from .views import bookSearchView

app_name = 'story8'

urlpatterns = [
    path('', views.bookSearchView, name='bookSearch'),
    path('ajax/', views.cobaAjax, name='ajax')
]