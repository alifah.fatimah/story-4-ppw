from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .views import bookSearchView
from story8 import views


class UrlsTest(TestCase):
    def test_bookSearch_use_right_function(self):
        self.bookSearch = reverse("story8:bookSearch")
        found = resolve(self.bookSearch)
        self.assertEqual(found.func, bookSearchView)
    def test_index_url(self):
        found = resolve(reverse('story8:ajax'))
        self.assertTrue(found.func, views.cobaAjax)

class ViewsTest(TestCase):
    def test_GET_bookSearch(self):
        self.bookSearch = reverse("story8:bookSearch")
        response = self.client.get(self.bookSearch)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'booksearch.html')

