from django.shortcuts import render
from django.http import JsonResponse
import requests as r

# Create your views here.
def bookSearchView(request):
    return render(request, 'booksearch.html')

def cobaAjax(request, book_name, start_index):
    url = "https://www.googleapis.com/books/v1/volumes?q="
    a = r.get(url)
    return JsonResponse(a.json()) #hasil dari parse akan dimasuukkan ke JResponse dan dikembalikan dair dictionary ke bentuk json lagi
